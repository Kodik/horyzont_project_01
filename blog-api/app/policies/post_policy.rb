class PostPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(active: true)
      end
    end
  end

  def index?
    user.admin? || user.visitor?
  end
end
