class User < ApplicationRecord
  has_many :posts, dependent: :destroy

  validates :login, presence: true
  validates :email, presence: true
  validates :role, presence: true

  enum role: %i[visitor admin]
end
