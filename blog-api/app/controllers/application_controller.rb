class ApplicationController < ActionController::API
  include Pundit
  include Response
  include ExceptionHandler
end
