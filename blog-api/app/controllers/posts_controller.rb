class PostsController < ApplicationController
  def index
    authorize :post, :index?
    redner jsonapi: policy_scope(Post).page(1), include: :user
  end

  def show

  end

  def create

  end

  def update

  end

  def destroy

  end
end
