FactoryBot.define do
  factory :admin do
    login { 'Kodik' }
    email { 'kodik@example.com' }
    role { :admin }
    first_name { 'Konrad' }
    last_name { 'Badzioch' }
  end

  FactoryBot.define do
    factory :user do
      login { 'User' }
      email { 'user@example.com' }
      role { :visitor }
      first_name { 'Example' }
      last_name { 'Example' }
    end
  end
end