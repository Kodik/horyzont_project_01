FactoryBot.define do
  factory :admin_post do
    title { 'Example title' }
    content { 'Example of content' }
    active { true }
  end
end