require 'rails_helper'

RSpec.describe User, type: :model do
  # Testing associations
  it { should have_many(:posts).dependent(:destroy) }

  # Testing validations
  it { should validate_presence_of(:login) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:role) }
end
