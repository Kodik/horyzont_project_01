class CreatePosts < ActiveRecord::Migration[5.2]
  def up
    create_table :posts do |t|
      t.references  :user,      null: false, foreign_key: true
      t.string      :title
      t.text        :content
      t.boolean     :active,    null: false, default: false
      t.timestamps              null: false
    end
  end

  def down
    drop_table :posts
  end
end
