# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :login,              null: false, default: ""
      t.string :email,              null: false, default: ""
      t.string :password_digest,    null: false, default: ""
      t.integer :role,              null: false, default: 0
      t.string :first_name
      t.string :last_name
      t.timestamps                  null: false
    end

    add_index :users, :email,                unique: true
    add_index :users, :login,                unique: true
  end
end
