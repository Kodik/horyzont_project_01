Rails.application.routes.draw do
  scope :api, defaults: { format: :json } do
    get '/users', to: 'users#index'
    resource :posts, only: %i[index create update destroy show]
  end
end
